//
// Created by bledar on 10/31/16.
//

#ifndef HOMEWORK1_HASHER_H
#define HOMEWORK1_HASHER_H
// The abstract base class of Hasher

// The capacity needs to get changed by the client who uses the hash table function.
// The change goes in accordance with the nature of the hasher function.

const int CAPACITY = 9;

template <typename K>
class Hasher
{
public:
    virtual int HashFunction(const K& k) const = 0;
};

// For simplicity, we will include the class that is inhereted from Hasher
// in this file.

// This would work for ints, for example. We will specifically specialize
// it below.

class SimpleIntHasher : public Hasher<int>
{
public:
    // Replace this function with any more sophistacated ones if need be.
    virtual int HashFunction(const int& k) const
    {
        return k% CAPACITY;
    }
};
#endif //HOMEWORK1_HASHER_H
