//
// Created by bledar on 10/31/16.
//

#ifndef HOMEWORK1_LINKEDLIST_H
#define HOMEWORK1_LINKEDLIST_H

#include "DNode.h"
#include "Iterator.h"
#include <iostream>

// This is the singly linked list.
template <typename T>
class LinkedList
{
protected:
    DNode<T>* _dhead; // Needed for efficiently adding/dropping elements from the front
    DNode<T>* _dtail; // Needed for efficiently adding/dropping elements from the back
    int _dcount;		// Needed for the size of the list


    // Private function to push from the front. This was for testing purposes.
    // In addition, in case it is needed later than we have it.
    virtual void push_front(T x)
    {
        DNode<T>* new_dnode = new DNode<T>(x);
        if (_dhead == nullptr) {
            _dhead = new_dnode;
            _dtail = new_dnode;
        }
        else {
            new_dnode->_next = _dhead;
            _dhead = new_dnode;
        }
        _dcount++;
    }
    // Private function to pop from the front. This will be used in the destructor
    void pop_front()
    {
        if (_dhead != nullptr)
        {
            DNode<T>* new_dhead = _dhead->_next;
            delete _dhead;
            _dhead = new_dhead;
        }
        _dcount--;
    }

public:
    LinkedList() :_dhead(nullptr),_dtail(nullptr),_dcount(0) {}

    ~LinkedList() { clear(); }

    // Function to clear the list, i.e. delete all nodes
    void clear()
    {
        while (_dcount > 0) {
            this->pop_front();
        }
    }

    virtual void Add(T x){
        DNode<T>* new_dnode = new DNode<T>(x);
        if (_dhead == nullptr)
        {
            _dhead = new_dnode;
            _dtail = new_dnode;
        }
        else{
            _dtail->_next = new_dnode;
            _dtail = new_dnode;
        }
        _dcount++;
    }

    // Inserts the value x at the position provided.
    virtual void Insert(T x, int pos)
    {
        if (_dhead == nullptr && pos != 1)
            std::cout << "Your list is empty and you can insert at position 1 only\n";
        else
        {
            if (pos > _dcount + 1)
                std::cout << "Your list has " << _dcount << " elements. You can enter data up to " << _dcount + 1 << "-th position.\n";
            else
            {
                if (pos == 1) Add(x);	// When the list is empty, then pos must be 1. In that case, just add it.
                else
                {
                    DNode<T>* current = new DNode<T>(x);
                    DNode<T>* previous = _dhead;		// Starts with the head's _next, i.e. points to the first element.
                    DNode<T>* after = _dhead->_next;	// Continues with the first element's _next, i.e. points to the second element.
                    for (int i = 1; i <= pos-2; ++i)	//
                    {
                        previous = previous->_next;
                        after = after->_next;
                    }
                    previous->_next = current;
                    current->_next = after;
                }
            }
        }
    }

    T Remove(int index)
    {
        T returned_value;
        if (_dhead == nullptr) {std::cout << "The list has no elements.\n"; returned_value= _dhead->_data;} // Returns 00000
        else
        {
            if (index > _dcount) { std::cout << "The index you provided is beyond the last element of the list.\n"; returned_value = _dhead->_data; }
            else
            {
                if (index == 1) { auto temp = Get(1); this->pop_front(); returned_value = temp; }
                else
                {
                    DNode<T>* previous = _dhead;		// Start with the pointer to the first element of the list
                    DNode<T>* current = _dhead->_next;	// Start with the pointer to the second element of the list
                    DNode<T>* after = _dhead->_next->_next;
                    int counter = 0;
                    for (int i = 1; i < index - 1; ++i)
                    {
                        previous = previous->_next;		// Iterating through the list
                        current = current->_next;		//       -   ||   -
                        after = after->_next;			//       -   ||   -
                    }
                    //if (i == index - 1)
                    returned_value = current->_data;
                    delete current;
                    previous->_next = after;
                }
            }
        }
        return returned_value;
    }

    // Returns the data at the specified index
    T Get(int index)
    {
        T returned_value;
        if (index < 1 || index > _dcount)
        {
            std::cout << "The index must be between 0 and the size of the list, " << _dcount << std::endl;
            returned_value = -1;
        }
        else
        {
            DNode<T>* current = _dhead;	// Points to the first element
            for (int j = 1; j < index; ++j)
            {
                current = current->_next;
            }
            returned_value = current->_data;
        }
        return returned_value;
    }

    // Returns the index of the specified element. If the element is not found, then
    // it returns -1.

    int IndexOf(T x)
    {
        if (_dhead == nullptr) return -1; // Since the list is empty
        else
        {
            int index = 0;
            DNode<T>* current = _dhead;
            while (current != nullptr)
            {
                index++;
                if (current->_data == x) return index;
                current = current->_next;
            }
            return -1;
        }
    }

    // Remove function: Removes the element at the specified index. In addition, we return
    // the element that was removed.


    // Displays the elements of the list.
    virtual void Display(){
        if (_dhead==nullptr)
            std::cout << "The list is empty\n";
        else{
            DNode<T>* current = _dhead;
            while (current !=nullptr){
                std::cout << current->_data<<" ";
                current = current->_next;
            }
            std::cout << "\n";
        }
    }

    int size(){
        return _dcount;
    }

    ListIterator<T> Iterator() { return ListIterator<T>(_dhead); }


};
#endif //HOMEWORK1_LINKEDLIST_H
