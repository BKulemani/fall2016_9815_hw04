//
// Created by bledar on 11/1/16.
//

#ifndef HOMEWORK_DOUBLYLINKEDLIST_H
#define HOMEWORK_DOUBLYLINKEDLIST_H

#include "LinkedList.h"
#include "DNode.h"

template <typename T>
class DoublyLinkedList : public LinkedList<T>
{
private:

    virtual void push_front(T x)
    {
        DNode<T>* new_dnode = new DNode<T>(x);
        if (this->_dhead == nullptr) { this->_dhead = new_dnode; this->_dtail = new_dnode; }
        else {
            new_dnode->_next = this->_dhead;
            (this->_dhead)->_previous = new_dnode;
            this->_dhead = new_dnode;}
        this->_dcount++;
    }

public:
    DoublyLinkedList() : LinkedList<T>() {}	// Calling the constructor of the base class

    /*		For the destructor, there is no need to add a body function since this class
        has no new members. When the object goes out of score, the destructor of the base
        class will be called, thus freeing the memory.
    */
    ~DoublyLinkedList() { /*std::cout << "Double destructor\n";*/ }

    virtual void Add(T x) {
        DNode<T>* new_dnode = new DNode<T>(x);
        if (this->_dtail == nullptr)
        {
            this->_dhead= new_dnode;
            this->_dtail= new_dnode;
        }
        else {
            (this->_dtail)->_next = new_dnode;
            new_dnode->_previous = this->_dtail;
            this->_dtail = new_dnode;
        }
        this->_dcount++;
    }

    // Inserts the value x at the position provided.
    // If the index is 1 or less, then it inserts in front.
    // If the index is greater than the size of the list, then it inserts in the back
    // ignoring the position.
    virtual void Insert(T x, int pos)
    {
        DNode<T>* current = new DNode<T>(x);
        if (pos <= 1) {	push_front(x);	}	// Adds in front
        else if (pos > this->_dcount) { Add(x); }	// Adds in the back
        else
        {	DNode<T>* prior = this->_dhead;
            DNode<T>* after = (this->_dhead)->_next;
            for (int i = 1; i <= pos - 2; ++i)
            {
                prior = prior->_next;
                after = after->_next;
            }
            prior->_next = current;
            current->_next = after;
            after->_previous = current;
            this->_dcount++;}
    }

    // Remove function
    T Remove(int index)
    {
        T returned_value;
        if (this->_dhead == nullptr) { returned_value = (this->_dhead)->_data; } // Returns 00000
        if (this->_dcount == 1) { returned_value = this->Get(1); this->pop_front(); }	// Gets the element in the first position, regardless of index, and then pops it, reducing the size by 1, making it 0.
        else
        {
            if (index <= 1)
            {
                auto temp = this->Get(1);
                this->pop_front();
                returned_value = temp;
            }	// Returns the value of the first element and then pops it.

            if (index >= this->_dcount)
            {
                auto temp = this->Get(this->_dcount);
                returned_value = temp;
                DNode<T>* new_tail = (this->_dtail)->_previous;
                new_tail->_next = nullptr;	// Since the new tail doesn't have a next
                delete this->_dtail;
                this->_dtail = new_tail;
                this->_dcount--;
            }
            else
            {
                DNode<T>* prior = this->_dhead;		// Start with the pointer to the first element of the list
                DNode<T>* current = (this->_dhead)->_next;	// Start with the pointer to the second element of the list
                DNode<T>* after = (this->_dhead)->_next->_next;
                int counter = 0;
                for (int i = 1; i < index - 1; ++i)
                {
                    prior = prior->_next;		// Iterating through the list
                    current = current->_next;		//       -   ||   -
                    after = after->_next;			//       -   ||   -
                }
                returned_value = current->_data;
                delete current;
                prior->_next = after;
                after->_previous = prior;
            }
        }
        return returned_value;
    }

    ListIterator<T> IteratorBack() { return ListIterator<T>(this->_dtail); }
};

#endif //HOMEWORK_DOUBLYLINKEDLIST_H
