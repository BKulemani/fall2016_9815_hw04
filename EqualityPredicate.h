//
// Created by bledar on 10/31/16.
//

#ifndef HOMEWORK1_EQUALITYPREDICATE_H
#define HOMEWORK1_EQUALITYPREDICATE_H

// Note that EqualityPredicate has pure virtual functions, making the class
// abstract.
// We will implement the astract base class and its derived class in the same
// header file for simplicity.

template <typename K>
class EqualityPredicate
{
public:
    virtual bool Equal(const K& k1, const K& k2) const = 0;
};


class SimpleIntEqualityPredicate : public EqualityPredicate<int>
{
public:
    virtual bool Equal(const int& k1, const int& k2) const {
        return k1 == k2;
    }
};
#endif //HOMEWORK1_EQUALITYPREDICATE_H
