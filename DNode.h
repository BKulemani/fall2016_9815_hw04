//
// Created by bledar on 10/31/16.
//

#ifndef HOMEWORK1_DNODE_H
#define HOMEWORK1_DNODE_H
#include "Node.h"

// Dnode class
// Note:
// In the hw it was mentioned  that we derive Dnode from Node. However,
// I did not do that for two reasons:
// 1) The solution would be a little more complicated
// 2) Second, I did not think having it inhereted would be substiantly beneficial.
// From now on, the linked lists (singly, or doubly) will have Dnode as one of
// their data members.
// However, the doubly linked list is inhereted from the singly linked list. This
// is because some of the functions in the singly-linked lists are in common with
// doubly-linked lists. Thus, we save time and effort by doing this.
template <typename T>
class DNode
{
public:
	T		 _data;
	DNode<T>* _previous;
	DNode<T>* _next;
	DNode(T d, DNode<T>* n = nullptr, DNode<T>* p = nullptr) : _previous(p), _next(n), _data(d) {}
};

#endif //HOMEWORK1_DNODE_H
