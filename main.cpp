#include "HashTable.h"
#include "LinkedList.h"
#include "DoublyLinkedList.h"
#include <iostream>

int main() {
    std::cout << "*******************************************\n";
    std::cout << "***   Exercise 1: Singly Linked Lists   ***\n";
    std::cout << "*******************************************\n";

    LinkedList<int> x;
    // Test 1: Adding elements to the list
    x.Add(10);
    x.Add(20);
    x.Add(30);
    x.Add(40);
    x.Add(50);
    // Displaying the elements
    std::cout << "Initially the list is:\n";
    std::cout << "\t";  x.Display();
    std::cout << std::endl << std::endl;


    // Test 2: Inserting an element at a specified index; indices are 1-based.
    // Enter 100 at the index 2
    x.Insert(100, 2);
    std::cout << "Entering 100 in location 2, the list becomes:\n";
    std::cout << "\t";  x.Display();
    std::cout << std::endl << std::endl;


    // Test 3: Getting the element at the specified index/
    // Say, the index = 5.
    std::cout << "The element at position 5 is: " << x.Get(5);
    std::cout << std::endl << std::endl;


    // Test 4: Returning the index of an element.
    // The index of an existing element in the list:
    std::cout << "The position of 50 is: " << x.IndexOf(50);
    std::cout << std::endl << std::endl;

    // The index of a non-existing element in the list:
    std::cout << "The position of 200 is: " << x.IndexOf(200);
    std::cout << std::endl << std::endl;


    // Test 5: Remove an element from the list. This will return that element also.
    // Removing the element with index 4 from the list.
    auto removed_element = x.Remove(4);
    std::cout << "The element removed, with index = 4, is: " << removed_element << std::endl;
    std::cout << "At this point, the list is:\n";
    std::cout << "\t"; x.Display();
    std::cout << std::endl << std::endl;


    // Test 6: Returning an iterator on the list.
    // The iterator is an object of the class ListIterator. Its data member a list head node.
    // We will use the iterator to loop in the list, and print the values in the list.
    std::cout << "Printing the list elements using the iterator:\n";
    for (auto it = x.Iterator(); it != nullptr; ++it)
    {
        std::cout << *it << "\t";
    }
    std::cout << std::endl << std::endl;

    // Test 7: Finding the size of the list
    std::cout << "The size of the list is: " << x.size();
    std::cout << std::endl << std::endl;
    std::cout << std::endl << std::endl;

    // Test 8: Testing the methods of the ListIterator
    std::cout << "New List\n";
    std::cout << "--------\n";
    LinkedList<int> y;
    y.Add(100);

    std::cout << "The original list is:\n";
    std::cout << "\t"; y.Display();
    std::cout << std::endl << std::endl;

    auto it2 = y.Iterator();
    std::cout << "Head node of this list has a non-null next pointer: " << std::boolalpha << it2.HasNext() << std::endl;
    std::cout << "Head first element of this list has a non-null next pointer: " << std::boolalpha << (++it2).HasNext() << std::endl;
    std::cout << std::endl << std::endl;

    auto it3 = y.Iterator();
    std::cout << "The next element of this iterator is: " << (it3++).Next() << std::endl;		// Post increment. Should return 1
    std::cout << "The next element of the next element of this iterator is: " << it3.Next() << std::endl;	// Should return 0; its design is such.

    std::cout << std::endl << std::endl << std::endl;
    std::cout << "*******************************************\n";
    std::cout << "***   Exercise 2: Doubly Linked Lists   ***\n";
    std::cout << "*******************************************\n";
    std::cout << std::endl << std::endl;

    DoublyLinkedList<int> l;
    // Test 1: Adding elements to the list
    for (int i = 1; i <= 7; ++i)
        l.Add(i);

    // Displaying the elements
    std::cout << "Initially the list is:\n";
    std::cout << "\t";  l.Display();
    std::cout << std::endl << std::endl;


    // Test 2: Inserting an element at a specified indel; indices are 1-based.
    // Enter 10 at the indel 2
    l.Insert(10, 2);
    std::cout << "Entering 10 in location 2, the list becomes:\n";
    std::cout << "\t";  l.Display();
    std::cout << std::endl << std::endl;


    // Test 3: Getting the element at the specified index
    // Say, the indel = 5.
    std::cout << "The element at position 5 is: " << l.Get(5);
    std::cout << std::endl << std::endl;


    // Test 4: Returning the indel of an element.
    // The indel of an elisting element in the list:
    std::cout << "The position of 5 is: " << l.IndexOf(5);
    std::cout << std::endl << std::endl;


    // The indel of a non-elisting element in the list:
    std::cout << "The position of 200 is: " << l.IndexOf(200);
    std::cout << std::endl << std::endl;


    // Test 5: Remove an element from the list. This will return that element also.
    // Removing the element with indel 4 from the list.
    auto removed_element2= l.Remove(4);
    std::cout << "The element removed, with index = 4, is: " << removed_element2 << std::endl;
    std::cout << "At this point, the list is:\n";
    std::cout << "\t"; l.Display();
    std::cout << std::endl << std::endl;


    // Test 6: Returning an iterator on the list.
    // The iterator is an object of the class ListIterator. Its data member a list head node.
    // We will use the iterator to loop in the list, and print the values in the list.
    std::cout << "Printing the list elements using the iterator:\n";
    for (auto it = l.Iterator(); it != nullptr; ++it)
    {
        std::cout << *it << "\t";
    }
    std::cout << std::endl << std::endl;

    // Test 7: Returning an iterator on the list.
    // In this case, we will return an end-of-list iterator, and loop backwards.
    DoublyLinkedList<int> l1;
    // Test 1: Adding elements to the list
    for (int i = 1; i <= 10; ++i)
        l1.Add(i);
    std::cout << "New List:\n"; l1.Display();
    std::cout << std::endl;
    std::cout << "Printing the list elements using the iterator_back:\n";
    for (auto it = l1.IteratorBack(); it != nullptr; --it)
    {
        std::cout << *it << " ";
    }
    std::cout << std::endl << std::endl;


    // Test 8: Finding the size of the list
    std::cout << "The size of the list is: " << l1.size();
    std::cout << std::endl << std::endl;
    std::cout << std::endl << std::endl;

    // Test 9: Testing the methods of the ListIterator
    std::cout << "New List\n";
    std::cout << "--------\n";
    LinkedList<int> l2;
    l2.Add(100);

    std::cout << "The original list is:\n";
    std::cout << "\t"; l2.Display();
    std::cout << std::endl << std::endl;

    auto it4 = l2.Iterator();
    std::cout << "Head node of this list has a non-null nelt pointer: " << std::boolalpha << it4.HasNext() << std::endl;
    std::cout << "Head first element of this list has a non-null nelt pointer: " << std::boolalpha << (++it4).HasNext() << std::endl;
    std::cout << std::endl << std::endl;

    auto it5 = l2.Iterator();
    std::cout << "The next element of this iterator is: " << (it5++).Next() << std::endl;		// Post increment. Should return 1
    std::cout << "The next element of the next element of this iterator is: " << it5.Next()<< std::endl;	// Should return 0; its design is such.
    std::cout << std::endl << std::endl << std::endl;

    std::cout << "*******************************************\n";
    std::cout << "****       Exercise 3: Hashtables      ****\n";
    std::cout << "*******************************************\n";

    // In our example, we will pick a capacity equal to 9.
    // We will add the values with keys = 0,1,2,..., 19.

    HashTable<int, int> myHashTable(HasherClassPointer<int>(new SimpleIntHasher), EqualityPredicateClassPointer<int>(new SimpleIntEqualityPredicate));
    for (int i = 0; i < 20; ++i)
        myHashTable.Add(i, i*i);

    // Test 1. Let's print the hashtable
    // We will analyze 3 pairs of numbers:
    // (4,16) and (13,139) should be placed in the same linked list. That is because, both
    // 4 and 13 = 4(mod 9). Furthemore, (13,139) should be after (4,16), since the latter is
    // added first.
    myHashTable.Print();
    std::cout << std::endl << std::endl;

    // Test 2. Let's get a value with key = 7;
    int key = 7;
    std::cout << "The value of the key " << key << " is " << *(myHashTable.GetValue(7)) << std::endl;
    std::cout << std::endl << std::endl;

    // Test 3. Remove a value with key = 10
    // We then can print the data structure to visuallize what happens.
    myHashTable.Remove(10);
    std::cout << "After removing the pair (10,100), the hash table becomes:\n";
    myHashTable.Print();
    std::cout << std::endl << std::endl;
    // Note that the pair (10,100) is deleted and the pair (19,361) is pushed to the left.
    return 0;
}