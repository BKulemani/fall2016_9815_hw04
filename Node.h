//
// Created by bledar on 10/31/16.
//

#ifndef HOMEWORK1_NODE_H
#define HOMEWORK1_NODE_H
// Node Class
template <typename T>
class Node
{
public:
	T _data;
	Node<T>* _next;
	Node<T>(T d, Node<T>* n = nullptr) : _data(d), _next(n) {}
};
#endif //HOMEWORK1_NODE_H
