//
// Created by bledar on 10/31/16.
//

#ifndef HOMEWORK1_HASHTABLE_H
#define HOMEWORK1_HASHTABLE_H
#include "Hasher.h"
#include "EqualityPredicate.h"
#include <memory>		// Neeed for smart pointers
#include <utility>		// Needed for std::pair
#include <array>		// Needed to store the lists
#include <list>			// Needed to create and maintain the lists
#include <algorithm>	// Needed to apply algos to containers
#include <iomanip>
#include <iostream>

// The implementation for this class, too, will be in the header file.
// The reason for this, is just simplicity. Normally, we would sepereate
// the decleration of the class and its implementation.

// Idea:
// We build the singly and doubly lists the problems 1 and 2. Dealing with
// the pointers, was not an easy task. Also, out lists are far from efficient
// when compared to the std::list. Therefore, to avoid these two "pain-in-the-neck"
//  issues, we will use std containers and shared pointers. This way, additionally,
// will make the code easy to follow.

// We also need pointers to the HasherFunction and the EqualityPredicate.
// For this, we use smart pointers, particulary shared pointers. This, way,
// the memory is self-managed.
template <typename K>
using HasherClassPointer = std::shared_ptr<Hasher<K>>;

template <typename K>
using EqualityPredicateClassPointer = std::shared_ptr<EqualityPredicate<K>>;


template <typename K, typename V>
class HashTable {
    // Somehow, we need to store the key and the value associated with it.
    // std provides us with a struct that allows us to store two heterogeneous
    // objects.
    using _key_value_pair = std::pair<K, V>;
    using _list_of_key_value_pair = std::list<_key_value_pair>;

    // Since our hash table is an array of linked lists, we need to construct
    // an array. The size of the array, however, depends on the capacity
    // provided by te user in the hash function.
    std::array<_list_of_key_value_pair, CAPACITY> HashTableArray;

    HasherClassPointer<K> _hasher;                            // The shared_pointer data member
    EqualityPredicateClassPointer<K> _equality_predicator;    // The shared_pointer data member

public:
    // Constructor
    // It will use:
    //	a)		The hash function coming from the Hasher class. Since Hasher is abstract, it
    //		will use the derived class, SimpleIntHasher, and use that function.
    //	b)		The Equal function coming from the EqualityPredicate class. Since EqualityPredicate
    //		is abstract class, it will use the derived class SimpleEqualityPredicate
    HashTable(HasherClassPointer<K> input_hasher, EqualityPredicateClassPointer<K> input_predicate)
            : _hasher(input_hasher), _equality_predicator(input_predicate) {
        ;
    }

    // Destructor
    ~HashTable() { ; }
    // No need for special attention . Nothing here is being created with "new"
    // Also, the pointers that are being created via "new" are shared. They take
    // care of themselves.

    // Printing a hash table in the shape they "saved"
    void Print() const {
        for (int i = 0; i < CAPACITY; ++i) {
            auto row1 = HashTableArray[i];
            for (auto it = row1.begin(); it != row1.end(); ++it) {
                std::cout << std::right << std::setw(8);
                std::cout << "(" << (*it).first << ", " << (*it).second << ")";
            }
            std::cout << std::endl;
        }
    }


    // Adding a value to the hash table. The user provides the key-value pair.
    void Add(const K &key, const V &value) {
        // We first need to know where in the array is the hash-value corresponding to our key.
        // For this, we can simply call the hash function using the key as the parameter. The
        // function will return the position. If we know the position, then we know the list in which
        // we need to enter the new std::pair(key, value).

        int position = _hasher->HashFunction(key);
        auto new_pair = std::make_pair(key, value);
        HashTableArray[position].push_back(new_pair);
    }

    // Remove the key-value pari from the hash table, given the key.
    // If the key doesn't exist in the table, then it does nothing.
    void Remove(const K &key) {
        // If the key-value doesn't exist, then nothing happes. If exist, then to delete, we proceed as follows:
        //	a)	We use the hasher function to tell us in what list is our key_value pair found.
        //		Note however, that the word "found" inherits its meaning from the definition of equalpredicate.
        //		If the predicate checks for a=b, then this is simply "equal to each other" and it is found.
        //		But, if the predicate checks for a*a=b, then this is simply "square of a is b" and it is found.
        //		Thus, the meaning "found" is based on the definition of the equal predicate.
        //	b)  Once, we find the position, then we just simply use the function erase() of std::list.

        int position = _hasher->HashFunction(key);
        auto pointer_to_key = std::find_if(HashTableArray[position].begin(), HashTableArray[position].end(),
                                           [&](_key_value_pair const &iterated_value) {
                                               return _equality_predicator->Equal(iterated_value.first,
                                                                                  key);    // Compares current value (first of the pair) with the current key
                                           }
        );
        if (pointer_to_key !=
            HashTableArray[position].end())    // This is the case where the key is found before the end is reached.
            HashTableArray[position].erase(pointer_to_key);
    }

    // This function returns the value associated with the key provided.
    // Again here, just like in the case of Remove, the meaining of "found" is subjected to the definition of the predicate.
    // There is a slight problem here. If the key is found, then we return that value. However, if the key is not found, what
    // do we return? To leave this up to the client, we just return a pointer. In case the key is found, we return the pointer
    // to the second element of the std::pair. Then, the user can just dereference it.

    V *GetValue(const K &key) {
        int position = _hasher->HashFunction(key);
        auto pointer_to_key = std::find_if(HashTableArray[position].begin(), HashTableArray[position].end(),
                                           [&](_key_value_pair const &iterated_value) {
                                               return _equality_predicator->Equal(iterated_value.first, key);
                                           }
        );
        if (pointer_to_key == HashTableArray[position].end())
            return NULL;
        else {
            auto temp1 = *pointer_to_key;
            auto temp2 = temp1.second;
            auto address = &temp2;
            return address;
        }
    }
};
#endif //HOMEWORK1_HASHTABLE_H
