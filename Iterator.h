//
// Created by bledar on 10/31/16.
//

#ifndef HOMEWORK1_ITERATOR_H
#define HOMEWORK1_ITERATOR_H
#include "DNode.h"
//#include "DoublyLinkedList.h"
#include "LinkedList.h"
// Iterator class

template <typename T>
class ListIterator
{
private:
	DNode<T>* _pointer_to_dnode;

public:
	//ListIterator() {}

	ListIterator(DNode<T>* x = nullptr) :_pointer_to_dnode(x) {}
	~ListIterator() {}

	// Pre-increment
	ListIterator<T>& operator++()
	{
		if (_pointer_to_dnode == nullptr) { return *this; }
		else {
			_pointer_to_dnode = _pointer_to_dnode->_next;
			return *this;
		}
	}

	// Post-increment
	ListIterator<T> operator++(int)
	{
		if (_pointer_to_dnode == nullptr) { return *this; }
		else {
			ListIterator<T> temp(*this);
			_pointer_to_dnode = _pointer_to_dnode->_next;
			return temp;
		}
	}

	// Pre-decrement
	ListIterator<T>& operator--()
	{// Maybe check whether _pointer_to_tail points to null
		if (_pointer_to_dnode == nullptr) { return *this; }
		else {
			_pointer_to_dnode = _pointer_to_dnode->_previous;
			return *this;
		}
	}

	// Post-decrement
	ListIterator<T> operator--(int)
	{// Maybe check whether _pointer_to_tail points to null
		if (_pointer_to_dnode == nullptr) { return *this; }
		else {
			ListIterator<T> temp(*this);
			_pointer_to_dnode = _pointer_to_dnode->_previous;
			return temp;
		}
	}

	// Not-equal operator
	bool operator!=(ListIterator<T> rval) { return !(_pointer_to_dnode == rval._pointer_to_dnode); }

	// Equal operator
	bool operator==(ListIterator<T> rval) { return _pointer_to_dnode == rval._pointer_to_dnode; }

	// Dereferencing
	T operator*()
	{
		if (_pointer_to_dnode == nullptr) { return 000000; }
		else
			return _pointer_to_dnode->_data;
	}

	// Returns true if the iterator points to a non-null pointer. False, otherwise.
	bool HasNext()
	{
		return (_pointer_to_dnode != nullptr);
	}

	// Returns the element that is pointed to by the iterator->next.
	// The convetion here, is that we print -1, if the
	T Next()
	{
		if (HasNext()) { return _pointer_to_dnode->_data; }
		else
		{
			return -1;
		}
	}
};
#endif //HOMEWORK1_ITERATOR_H
